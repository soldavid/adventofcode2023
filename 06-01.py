times = []
record_distances = []
wins = 1

with open("06-input.txt") as input_file:
    for line in input_file:
        line = line.strip()  # Remove leading/trailing whitespace
        kind, values_field = line.split(":")
        values = values_field.split()
        if kind == "Time":
            times = [int(time) for time in values]
        if kind == "Distance":
            record_distances = [int(record_distance) for record_distance in values]
        for race in zip(times, record_distances):
            winning = 0
            for speed in range(1, race[0]):
                distance = (race[0] - speed) * speed
                if distance > race[1]:
                    winning += 1
            wins = wins * winning
print(wins)
