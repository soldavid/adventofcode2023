map: list[str] = []
start_x = 0
start_y = 0
steps = 0
paths: list[str] = []
pipe: list[str] = []
positions: list[tuple[int, int]] = [(0, 0), (0, 0)]

# Directions are North(0), South(1), West(2), East(3), Advance Vertical and Horizontal
directions = {
    "N": (-1, 0),
    "S": (1, 0),
    "W": (0, -1),
    "E": (0, 1),
}

# If you come from N, S, W, E direction, where so you need to go?
pipes = {
    "|": {"S": "S", "N": "N"},
    "-": {"E": "E", "W": "W"},
    "L": {"S": "E", "W": "N"},
    "J": {"S": "W", "E": "N"},
    "7": {"N": "W", "E": "S"},
    "F": {"N": "E", "W": "S"},
}

# Reads series
with open("10-input.txt") as input_file:
    for line in input_file:
        line = line.strip()
        map.append(line)
        if "S" in line:
            start_x = line.index("S")
            start_y = len(map) - 1
    print(f"Starting position: ({start_y}, {start_x})")

# map.append("OF----7F7F7F7F-7OOOO")
# map.append("O|F--7||||||||FJOOOO")
# map.append("O||OFJ||||||||L7OOOO")
# map.append("FJL7L7LJLJ||LJIL-7OO")
# map.append("L--JOL7IIILJS7F-7L7O")
# map.append("OOOOF-JIIF7FJ|L7L7L7")
# map.append("OOOOL7IF7||L7|IL7L7|")
# map.append("OOOOO|FJLJ|FJ|F7|OLJ")
# map.append("OOOOFJL-7O||O||||OOO")
# map.append("OOOOL---JOLJOLJLJOOO")

# start_x = 12
# start_y = 4


# Check where the paths start from the origin
# Look North
if map[start_y - 1][start_x] in "|7F":
    paths.append("N")
# Look South
if map[start_y + 1][start_x] in "|LJ":
    paths.append("S")
# Look West
if map[start_y][start_x - 1] in "-FL":
    paths.append("W")
# Look East
if map[start_y][start_x + 1] in "-7J":
    paths.append("E")

positions[0] = (start_y, start_x)
positions[1] = (start_y, start_x)

while True:
    steps += 1
    print(f"Step {steps}")

    # Appends starting positions
    offset_y, offset_x = directions[paths[0]]
    positions[0] = (positions[0][0] + offset_y, positions[0][1] + offset_x)
    offset_y, offset_x = directions[paths[1]]
    positions[1] = (positions[1][0] + offset_y, positions[1][1] + offset_x)

    pipe.clear()
    pipe.append(map[positions[0][0]][positions[0][1]])
    pipe.append(map[positions[1][0]][positions[1][1]])

    # Prints starting positions
    print(f"Position 1: {positions[0]}; {paths[0]}; Pipe: {pipe[0]}")
    print(f"Position 2: {positions[1]}; {paths[1]}; Pipe: {pipe[1]}")

    paths[0] = pipes[pipe[0]][paths[0]]
    paths[1] = pipes[pipe[1]][paths[1]]

    print(f"New Paths: {paths}")

    if positions[0] == positions[1]:
        print(f"Positions are the same, finished after {steps} steps.")
        break

print(f"{steps=}")
