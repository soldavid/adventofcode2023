limit = {
    "red": 12,
    "green": 13,
    "blue": 14,
}
total = 0
# Load the text file 02-input.txt and print each line
with open("02-input.txt") as input_file:
    for line in input_file:
        possible = True
        line = line.strip()  # Remove leading/trailing whitespace
        # Parse the game number
        game_number = int(line[5:].split(":")[0])
        # For each set
        for set in line.split(":")[1].split(";"):
            # For each color
            for color_record in set.split(","):
                color_record = color_record.strip()
                color_number, color_name = color_record.split(" ")
                if limit[color_name] < int(color_number):
                    possible = False
        if possible:
            total += game_number
print(total)
