hands: list[tuple[str, int]] = []
winnings: int = 0

card_numerical_value = {
    "A": 13,
    "K": 12,
    "Q": 11,
    "T": 10,
    "9": 9,
    "8": 8,
    "7": 7,
    "6": 6,
    "5": 5,
    "4": 4,
    "3": 3,
    "2": 2,
    "J": 1,
}

type_numerical_values = {
    "Five_of_a_kind": 7,
    "Four_of_a_kind": 6,
    "Full_house": 5,
    "Three_of_a_kind": 4,
    "Two_pairs": 3,
    "One_pair": 2,
    "High_card": 1,
}


# Function that return the numerical value of the hand parameter
def type_value(hand: str) -> int:
    # Get the count of each card in the hand
    card_times: dict[str, int] = {}
    for card in hand:
        card_times[card] = card_times.get(card, 0) + 1
    # Get the number of jokers out
    jokers = card_times.get("J", 0)
    # If only jokers, return Five of a kind
    if jokers == 5:
        return type_numerical_values["Five_of_a_kind"]
    # Remove the jokers from the hand
    if "J" in card_times:
        del card_times["J"]
    # Get the maximum of unique card in the hand
    max_times = max([times for times in card_times.values()])
    # Add the number of jokers to the first card with the max occurrences
    for key, value in card_times.items():
        if value == max_times:
            card_times[key] += jokers
            break
    # If there is only one unique card in the hand, it is a 5 of a kind
    if len(card_times) == 1:
        value = type_numerical_values["Five_of_a_kind"]
    else:
        # If there are two unique cards in the hand
        if len(card_times) == 2:
            # If one of the results is 4 or 1, it is a 4 of a kind
            _, times = card_times.popitem()
            if times in [1, 4]:
                value = type_numerical_values["Four_of_a_kind"]
            else:
                # Otherwise, it is a full house
                value = type_numerical_values["Full_house"]
        else:
            # If there are three unique cards in the hand
            if len(card_times) == 3:
                # If one of the results is 3, it is a three of a kind
                max_times = max([times for times in card_times.values()])
                if max_times == 3:
                    value = type_numerical_values["Three_of_a_kind"]
                else:
                    # Otherwise, it is a two pairs
                    value = type_numerical_values["Two_pairs"]
            else:
                # If there are four unique cards in the hand, it is a one pair
                if len(card_times) == 4:
                    value = type_numerical_values["One_pair"]
                else:
                    # Otherwise, it is a high card
                    value = type_numerical_values["High_card"]
    return value


# Function that calculates a strength numerical value for a hand, like it was base 14
# The type is the most important value
# Then the five cards value
# (Type * 14⁵) + (Card_1 * 14⁴) + (Card_2 * 14³) + (Card_3 * 14²) + (Card_4 * 14¹) + (Card_5 * 14⁰)
def hand_value(hand: str) -> int:
    base = 14
    exponential = 0
    value = 0
    # Add the value of each card
    for card in reversed(hand):
        value += card_numerical_value[card] * (base**exponential)
        exponential += 1
    # And the value of the type
    value += type_value(hand) * (14**5)
    return value


# Reads all hands
with open("07-input.txt") as input_file:
    for line in input_file:
        hand, bid = line.strip().split(" ")
        hands.append((hand, int(bid)))

# Test data
# hands = [
#     ("32T3K", 765),
#     ("T55J5", 684),
#     ("KK677", 28),
#     ("KTJJT", 220),
#     ("QQQJA", 483),
# ]

# Sort the data by hand value ascending
hands.sort(key=lambda x: hand_value(x[0]))

# Assigns the rank of each hand, and calculate winnings
for rank, (hand, bid) in enumerate(hands, start=1):
    # print(f"{rank=}; {hand=}; {bid=}; {rank*bid=}")
    winnings += rank * bid

print(f"{winnings=}")
