total = 0
# Load the text file 02-input.txt and print each line
with open("02-input.txt") as input_file:
    for line in input_file:
        power = 0
        minimum = {
            "red": 0,
            "green": 0,
            "blue": 0,
        }
        line = line.strip()  # Remove leading/trailing whitespace
        # For each set
        for set in line.split(":")[1].split(";"):
            # For each color
            for color_record in set.split(","):
                color_record = color_record.strip()
                color_number, color_name = color_record.split(" ")
                if minimum[color_name] < int(color_number):
                    minimum[color_name] = int(color_number)
        power = minimum["red"] * minimum["green"] * minimum["blue"]
        total += power
print(total)
