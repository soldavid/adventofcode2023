total = 0
schematic = []
# Load the text file 03-input.txt into the schematics array
with open("03-input.txt") as input_file:
    for line in input_file:
        line = line.strip()  # Remove leading/trailing whitespace
        schematic.append(line)
    max_x = len(schematic[0])
    max_y = len(schematic)
# for each line and column
y = 0
while y < max_y:
    x = 0
    while x < max_x:
        part = ""
        if schematic[y][x].isdigit():
            # Found the start of a possible part number
            part += schematic[y][x]
            part_start = x
            # Look for the rest of the part number
            x += 1
            while (x < max_x) and (schematic[y][x].isdigit()):
                part += schematic[y][x]
                x += 1
            # Found the whole possible part number
            part_length = len(part)
            is_part = False
            # print(part)
            # Now, if not on the first line
            if y > 0:
                # Tries to find a symbol on top
                for x_search in range(part_length):
                    if schematic[y - 1][part_start + x_search] != ".":
                        is_part = True
                        # print("top", schematic[y - 1][part_start + x_search])
                        break
                # If not found and not at the start of the line, tries to find a symbol before
                if not (is_part) and (part_start > 0):
                    if schematic[y - 1][part_start - 1] != ".":
                        is_part = True
                        # print("top before", schematic[y][part_start - 1])
                # If not found and not at the end of the line, tries to find a symbol after
                if not (is_part) and ((part_start + part_length) < max_x):
                    if schematic[y - 1][part_start + part_length] != ".":
                        is_part = True
                        # print("top after", schematic[y][part_start + part_length - 1])
            # If not found and not on the last line, tries to find a symbol on bottom
            if not (is_part) and (y < (max_y - 1)):
                for x_search in range(part_length):
                    if schematic[y + 1][part_start + x_search] != ".":
                        is_part = True
                        # print("bottom", schematic[y + 1][part_start + x_search])
                        break
                # If not found and not at the start of the line, tries to find a symbol before
                if not (is_part) and (part_start > 0):
                    if schematic[y + 1][part_start - 1] != ".":
                        is_part = True
                        # print("bottom before", schematic[y][part_start - 1])
                # If not found and not at the end of the line, tries to find a symbol after
                if not (is_part) and ((part_start + part_length) < max_x):
                    if schematic[y + 1][part_start + part_length] != ".":
                        is_part = True
                        # print("bottom after", schematic[y][part_start + part_length - 1])
            # If not found and not at the start of the line, tries to find a symbol before
            if not (is_part) and (part_start > 0):
                if schematic[y][part_start - 1] != ".":
                    is_part = True
                    # print("before", schematic[y][part_start - 1])
            # If not found and not at the end of the line, tries to find a symbol after
            if not (is_part) and ((part_start + part_length) < max_x):
                if schematic[y][part_start + part_length] != ".":
                    is_part = True
                    # print("after", schematic[y][part_start + part_length - 1])
            # If it is a part add it to the total
            if is_part:
                total += int(part)
        x += 1
    y += 1
print(total)
