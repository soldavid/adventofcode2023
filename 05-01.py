current_map = ""
seeds = []
maps = {}
min_location = -1

with open("05-input.txt") as input_file:
    for line in input_file:
        line = line.strip()  # Remove leading/trailing whitespace

        if ":" in line:
            kind, data = line.split(":")
            match kind:
                case "seeds":
                    # process seeds
                    seeds = data.split()
                case "seed-to-soil map":
                    current_map = "seed-to-soil"
                case "soil-to-fertilizer map":
                    current_map = "soil-to-fertilizer"
                case "fertilizer-to-water map":
                    current_map = "fertilizer-to-water"
                case "water-to-light map":
                    current_map = "water-to-light"
                case "light-to-temperature map":
                    current_map = "light-to-temperature"
                case "temperature-to-humidity map":
                    current_map = "temperature-to-humidity"
                case "humidity-to-location map":
                    current_map = "humidity-to-location"
        else:
            if line:
                destination, origin, range = line.split()
                origin_start = int(origin)
                origin_end = origin_start + int(range) - 1
                destination_offset = origin_start - int(destination)
                if current_map not in maps:
                    maps[current_map] = []
                maps[current_map].append((origin_start, origin_end, destination_offset))

for seed in seeds:
    seed = int(seed)
    soil = seed
    for range in maps["seed-to-soil"]:
        if seed >= range[0] and seed <= range[1]:
            soil = seed - range[2]
            break
    fertilizer = soil
    for range in maps["soil-to-fertilizer"]:
        if soil >= range[0] and soil <= range[1]:
            fertilizer = soil - range[2]
            break
    water = fertilizer
    for range in maps["fertilizer-to-water"]:
        if fertilizer >= range[0] and fertilizer <= range[1]:
            water = fertilizer - range[2]
            break
    light = water
    for range in maps["water-to-light"]:
        if water >= range[0] and water <= range[1]:
            light = water - range[2]
            break
    temperature = light
    for range in maps["light-to-temperature"]:
        if light >= range[0] and light <= range[1]:
            temperature = light - range[2]
            break
    humidity = temperature
    for range in maps["temperature-to-humidity"]:
        if temperature >= range[0] and temperature <= range[1]:
            humidity = temperature - range[2]
            break
    location = humidity
    for range in maps["humidity-to-location"]:
        if humidity >= range[0] and humidity <= range[1]:
            location = humidity - range[2]
            break
    if min_location == -1:
        min_location = location
    else:
        min_location = min(min_location, location)

print(min_location)
