map: list[str] = []
start_x = 0
start_y = 0
steps = 0
paths: list[str] = []
pipe: list[str] = []
positions: list[tuple[int, int]] = [(0, 0), (0, 0)]
borders: list[tuple[int, int]] = []

# Directions are North(0), South(1), West(2), East(3), Advance Vertical and Horizontal
directions = {
    "N": (-1, 0),
    "S": (1, 0),
    "W": (0, -1),
    "E": (0, 1),
}

# If you come from N, S, W, E direction, where so you need to go?
pipes = {
    "|": {"S": "S", "N": "N"},
    "-": {"E": "E", "W": "W"},
    "L": {"S": "E", "W": "N"},
    "J": {"S": "W", "E": "N"},
    "7": {"N": "W", "E": "S"},
    "F": {"N": "E", "W": "S"},
}

# Reads series
# with open("10-input.txt") as input_file:
#     for line in input_file:
#         line = line.strip()
#         map.append(line)
#         if "S" in line:
#             start_x = line.index("S")
#             start_y = len(map) - 1
#     borders.append((start_y, start_x))

# map.append("...........")
# map.append(".S-------7.")
# map.append(".|F-----7|.")
# map.append(".||.....||.")
# map.append(".||.....||.")
# map.append(".|L-7.F-J|.")
# map.append(".|..|.|..|.")
# map.append(".L--J.L--J.")
# map.append("...........")

# map.append("..........")
# map.append(".S------7.")
# map.append(".|F----7|.")
# map.append(".||OOOO||.")
# map.append(".||OOOO||.")
# map.append(".|L-7F-J|.")
# map.append(".|II||II|.")
# map.append(".L--JL--J.")
# map.append("..........")

map.append("OF----7F7F7F7F-7OOOO")
map.append("O|F--7||||||||FJOOOO")
map.append("O||OFJ||||||||L7OOOO")
map.append("FJL7L7LJLJ||LJIL-7OO")
map.append("L--JOL7IIILJS7F-7L7O")
map.append("OOOOF-JIIF7FJ|L7L7L7")
map.append("OOOOL7IF7||L7|IL7L7|")
map.append("OOOOO|FJLJ|FJ|F7|OLJ")
map.append("OOOOFJL-7O||O||||OOO")
map.append("OOOOL---JOLJOLJLJOOO")

start_x = 12
start_y = 4
borders.append((start_y, start_x))

# Check where the paths start from the origin
# Look North
if map[start_y - 1][start_x] in "|7F":
    paths.append("N")
# Look South
if map[start_y + 1][start_x] in "|LJ":
    paths.append("S")
# Look West
if map[start_y][start_x - 1] in "-FL":
    paths.append("W")
# Look East
if map[start_y][start_x + 1] in "-7J":
    paths.append("E")

positions[0] = (start_y, start_x)
positions[1] = (start_y, start_x)

borders.append(positions[0])

while True:
    steps += 1

    # Calculates new positions
    offset_y, offset_x = directions[paths[0]]
    positions[0] = (positions[0][0] + offset_y, positions[0][1] + offset_x)
    offset_y, offset_x = directions[paths[1]]
    positions[1] = (positions[1][0] + offset_y, positions[1][1] + offset_x)

    if positions[0] == positions[1]:
        borders.append(positions[0])
        break

    borders.append(positions[0])
    borders.append(positions[1])

    pipe.clear()
    pipe.append(map[positions[0][0]][positions[0][1]])
    pipe.append(map[positions[1][0]][positions[1][1]])

    paths[0] = pipes[pipe[0]][paths[0]]
    paths[1] = pipes[pipe[1]][paths[1]]

area = 0

# For each position in the map, check if it is inside or outside of the area
for y in range(len(map)):
    for x in range(len(map[0])):
        print(y, x)
        if (y, x) not in borders:
            # Checks if reaches north
            touches = 0
            for y_lookup in range(y - 1, -1, -1):
                if (y_lookup, x) in borders:
                    touches += 1
            if touches % 2 != 0:
                # Checks if reaches south
                touches = 0
                for y_lookup in range(y + 1, len(map[0]), 1):
                    if (y_lookup, x) in borders:
                        touches += 1
                if touches % 2 != 0:
                    # Checks if reaches west
                    touches = 0
                    for x_lookup in range(x - 1, -1, -1):
                        if (y, x_lookup) in borders:
                            touches += 1
                    if touches % 2 != 0:
                        # Checks if reaches east
                        touches = 0
                        for x_lookup in range(x + 1, len(map[0]), 1):
                            if (y, x_lookup) in borders:
                                touches += 1
                        if touches % 2 != 0:
                            area += 1

print(f"{area=}")
