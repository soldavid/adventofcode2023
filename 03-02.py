total = 0
schematic = []
# Load the text file 03-input.txt into the schematics array
with open("03-input.txt") as input_file:
    for line in input_file:
        line = line.strip()  # Remove leading/trailing whitespace
        schematic.append(line)
    max_x = len(schematic[0])
    max_y = len(schematic)
# for each line and column
y = 0
while y < max_y:
    x = 0
    while x < max_x:
        # Could be a gear?
        if schematic[y][x] == "*":
            # Clear numbers
            numbers = []
            # If not on the first line
            if y > 0:
                # Look for a number directly on top
                if schematic[y - 1][x].isdigit():
                    # Then there is one and only one number on top, get previous digits
                    number = ""
                    search_x = x - 1
                    while (search_x >= 0) and (schematic[y - 1][search_x].isdigit()):
                        number = schematic[y - 1][search_x] + number
                        search_x -= 1
                    # Add the digit on top
                    number += schematic[y - 1][x]
                    # Add following digits
                    search_x = x + 1
                    while (search_x < max_x) and (schematic[y - 1][search_x].isdigit()):
                        number += schematic[y - 1][search_x]
                        search_x += 1
                    # Add the number to the list
                    numbers.append(int(number))
                # Else there could be up to two numbers
                else:
                    # Look for a top left one
                    if schematic[y - 1][x - 1].isdigit():
                        # Then there is, get previous digits
                        number = ""
                        search_x = x - 2
                        while (search_x >= 0) and (
                            schematic[y - 1][search_x].isdigit()
                        ):
                            number = schematic[y - 1][search_x] + number
                            search_x -= 1
                        # Add the digit on top left
                        number += schematic[y - 1][x - 1]
                        # Add the number to the list
                        numbers.append(int(number))
                    # Look for a top right one
                    if schematic[y - 1][x + 1].isdigit():
                        # Then there is, add the digit on top left
                        number = ""
                        number += schematic[y - 1][x + 1]
                        # Add the next digits
                        search_x = x + 2
                        while (search_x < max_x) and (
                            schematic[y - 1][search_x].isdigit()
                        ):
                            number = number + schematic[y - 1][search_x]
                            search_x += 1
                        # Add the number to the list
                        numbers.append(int(number))
            # If not on the last line
            if y != (max_y - 1):
                # Look for a number directly on bottom
                if schematic[y + 1][x].isdigit():
                    # Then there is one and only one number on top, get previous digits
                    number = ""
                    search_x = x - 1
                    while (search_x >= 0) and (schematic[y + 1][search_x].isdigit()):
                        number = schematic[y + 1][search_x] + number
                        search_x -= 1
                    # Add the digit on top
                    number += schematic[y + 1][x]
                    # Add following digits
                    search_x = x + 1
                    while (search_x < max_x) and (schematic[y + 1][search_x].isdigit()):
                        number += schematic[y + 1][search_x]
                        search_x += 1
                    # Add the number to the list
                    numbers.append(int(number))
                # Else there could be up to two numbers
                else:
                    # Look for a top left one
                    if schematic[y + 1][x - 1].isdigit():
                        # Then there is, get previous digits
                        number = ""
                        search_x = x - 2
                        while (search_x >= 0) and (
                            schematic[y + 1][search_x].isdigit()
                        ):
                            number = schematic[y + 1][search_x] + number
                            search_x -= 1
                        # Add the digit on top left
                        number += schematic[y + 1][x - 1]
                        # Add the number to the list
                        numbers.append(int(number))
                    # Look for a top right one
                    if schematic[y + 1][x + 1].isdigit():
                        # Then there is, add the digit on top left
                        number = ""
                        number += schematic[y + 1][x + 1]
                        # Add the next digits
                        search_x = x + 2
                        while (search_x < max_x) and (
                            schematic[y + 1][search_x].isdigit()
                        ):
                            number = number + schematic[y + 1][search_x]
                            search_x += 1
                        # Add the number to the list
                        numbers.append(int(number))
            # Search for a number just before
            if schematic[y][x - 1].isdigit():
                # Then there is, get previous digits
                number = ""
                search_x = x - 1
                while (search_x >= 0) and (schematic[y][search_x].isdigit()):
                    number = schematic[y][search_x] + number
                    search_x -= 1
                # Add the number to the list
                numbers.append(int(number))
            # Search for a number just after
            if schematic[y][x + 1].isdigit():
                # Then there is, get next digits
                number = ""
                search_x = x + 1
                while (search_x < max_x) and (schematic[y][search_x].isdigit()):
                    number += schematic[y][search_x]
                    search_x += 1
                # Add the number to the list
                numbers.append(int(number))
            # If there were two numbers, it is a gear
            if len(numbers) == 2:
                ratio = numbers[0] * numbers[1]
                total += ratio
        x += 1
    y += 1
print(total)
