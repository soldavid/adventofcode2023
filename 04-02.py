total = 0
current_card = 1
# Record for each card, {card_number: copies}
cards = {}
# Load the text file 04-input.txt
with open("04-input.txt") as input_file:
    for line in input_file:
        line_value = 0
        line = line.strip()  # Remove leading/trailing whitespace
        numbers = line.split(":")[1].strip()
        winners_string, having_string = numbers.split("|")
        winners = winners_string.strip().split()
        having = having_string.strip().split()
        # Finds the items in the having list that exist in the winners list
        for item in winners:
            if item in having:
                line_value = 1 if line_value == 0 else line_value + 1
        # Add the original card
        copies = cards.get(current_card, 0) + 1
        total += copies
        # Add one to the copies of the next cards, depending on the wins
        for next_card in range(current_card + 1, current_card + line_value + 1):
            cards[next_card] = cards.get(next_card, 0) + copies
        current_card += 1
print(total)
