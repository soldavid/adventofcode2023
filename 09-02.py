values: list[list[int]]
sum_of_first_values = 0

# Reads series
with open("09-input.txt") as input_file:
    for line in input_file:
        line = line.strip()
        values = []
        values.append([int(value) for value in line.split(" ")])
        print(values[0])
        level = 1
        while True:
            only_zero = True
            values.append([])
            for index in range(1, len(values[level - 1])):
                difference = values[level - 1][index] - values[level - 1][index - 1]
                values[level].append(difference)
                if difference != 0:
                    only_zero = False
            print(values[level])
            if only_zero:
                break
            level += 1
        for current_level in range(level - 1, 0, -1):
            first_value = values[current_level][0]
            previous_value = values[current_level - 1][0] - first_value
            values[current_level - 1].insert(0, previous_value)
            print(
                f"Level {current_level}; First value {first_value}; Previous Element {previous_value}"
            )
        sum_of_first_values += values[0][0]
print(sum_of_first_values)
