instructions: str = ""
number_instructions: int = 0
nodes: dict[str, tuple[str, str]] = {}
steps: int = 0

direction_value: dict[str, int] = {
    "L": 0,
    "R": 1,
}

# Reads instructions and nodes
with open("08-input.txt") as input_file:
    for line in input_file:
        line = line.strip()
        # If not empty line
        if line:
            if "=" in line:
                # It is a node
                node, edges = line.split(" = ")
                left_edge, right_edge = edges[1:-1].split(", ")
                nodes[node] = (left_edge, right_edge)
            else:
                # It is the instructions
                instructions = line
                number_instructions = len(instructions)

# Start at node "AAA"
current_node = "AAA"
# Start at the first instruction
instruction_index = 0

# Reach node "ZZZ"
while current_node != "ZZZ":
    # Increment the number of steps
    steps += 1
    # Follow current instruction
    current_node = nodes[current_node][direction_value[instructions[instruction_index]]]
    # Increment the instruction index, returning to 0 if needed
    instruction_index = (instruction_index + 1) % number_instructions

print(f"{steps=}")
