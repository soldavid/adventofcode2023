# Load the text file 01-input.txt and print each line
total = 0
with open("01-input.txt") as input_file:
    for line in input_file:
        first_digit = ""
        last_digit = ""
        line = line.strip()  # Remove leading/trailing whitespace
        # Looks for the first digit in the line
        for character in line:
            if character.isdigit():
                first_digit = character
                break
        # Looks for the last digit in the line
        for character in reversed(line):
            if character.isdigit():
                last_digit = character
                break
        total += int(first_digit + last_digit)
print(total)
