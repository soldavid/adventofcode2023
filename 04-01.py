total = 0
# Load the text file 04-input.txt
with open("04-input.txt") as input_file:
    for line in input_file:
        line_value = 0
        line = line.strip()  # Remove leading/trailing whitespace
        numbers = line.split(":")[1].strip()
        winners_string, having_string = numbers.split("|")
        winners = winners_string.strip().split()
        having = having_string.strip().split()
        # Finds the items in the having list that exist in the winners list
        for item in winners:
            if item in having:
                line_value = 1 if line_value == 0 else line_value * 2
        total += line_value
print(total)
