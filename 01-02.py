# Stings to look for and their value
conversion = {
    "1": "1",
    "2": "2",
    "3": "3",
    "4": "4",
    "5": "5",
    "6": "6",
    "7": "7",
    "8": "8",
    "9": "9",
    "one": "1",
    "two": "2",
    "three": "3",
    "four": "4",
    "five": "5",
    "six": "6",
    "seven": "7",
    "eight": "8",
    "nine": "9",
}
total = 0
# Load the text file 01-input.txt and print each line
with open("01-input.txt") as input_file:
    for line in input_file:
        first_digit = ""
        last_digit = ""
        # Remove leading/trailing whitespace
        line = line.strip()
        new_line = ""
        line_length = len(line)
        position = 0
        # Transforms all the numbers in the line
        while position < line_length:
            found = False
            for key, value in conversion.items():
                if line[position:].startswith(key):
                    new_line += value
                    found = True
                    break
            position += 1
        print(line, new_line, new_line[0] + new_line[-1])
        total += int(new_line[0] + new_line[-1])
print(total)
