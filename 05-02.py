from typing import Dict, List, Tuple

current_map = ""
seeds = []
maps: Dict[str, List[Tuple[int, int, int]]] = {}
min_location = -1

with open("05-input.txt") as input_file:
    for line in input_file:
        line = line.strip()  # Remove leading/trailing whitespace

        if ":" in line:
            kind, data = line.split(":")
            match kind:
                case "seeds":
                    # process seeds
                    seed_ranges = data.split()
                    for i in range(0, len(seed_ranges), 2):
                        start = int(seed_ranges[i])
                        seed_number = int(seed_ranges[i + 1])
                        for seed in range(start, start + seed_number):
                            seeds.append(seed)
                case "seed-to-soil map":
                    current_map = "seed-to-soil"
                case "soil-to-fertilizer map":
                    current_map = "soil-to-fertilizer"
                case "fertilizer-to-water map":
                    current_map = "fertilizer-to-water"
                case "water-to-light map":
                    current_map = "water-to-light"
                case "light-to-temperature map":
                    current_map = "light-to-temperature"
                case "temperature-to-humidity map":
                    current_map = "temperature-to-humidity"
                case "humidity-to-location map":
                    current_map = "humidity-to-location"
        else:
            if line:
                destination, origin, range = line.split()
                origin_start = int(origin)
                origin_end = origin_start + int(range) - 1
                destination_offset = origin_start - int(destination)
                if current_map not in maps:
                    maps[current_map] = []
                maps[current_map].append((origin_start, origin_end, destination_offset))

for seed in seeds:
    seed = int(seed)
    soil = seed
    for seed_range in maps["seed-to-soil"]:
        if seed >= seed_range[0] and seed <= seed_range[1]:
            soil = seed - seed_range[2]
            break
    fertilizer = soil
    for soil_range in maps["soil-to-fertilizer"]:
        if soil >= soil_range[0] and soil <= soil_range[1]:
            fertilizer = soil - soil_range[2]
            break
    water = fertilizer
    for fertilizer_range in maps["fertilizer-to-water"]:
        if fertilizer >= fertilizer_range[0] and fertilizer <= fertilizer_range[1]:
            water = fertilizer - fertilizer_range[2]
            break
    light = water
    for water_range in maps["water-to-light"]:
        if water >= water_range[0] and water <= water_range[1]:
            light = water - water_range[2]
            break
    temperature = light
    for light_range in maps["light-to-temperature"]:
        if light >= light_range[0] and light <= light_range[1]:
            temperature = light - light_range[2]
            break
    humidity = temperature
    for temperature_range in maps["temperature-to-humidity"]:
        if temperature >= temperature_range[0] and temperature <= temperature_range[1]:
            humidity = temperature - temperature_range[2]
            break
    location = humidity
    for humidity_range in maps["humidity-to-location"]:
        if humidity >= humidity_range[0] and humidity <= humidity_range[1]:
            location = humidity - humidity_range[2]
            break
    if min_location == -1:
        min_location = location
    else:
        min_location = min(min_location, location)

print(min_location)
