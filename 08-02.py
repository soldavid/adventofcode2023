instructions: str = ""
number_instructions: int = 0
nodes: dict[str, tuple[str, str]] = {}
current_nodes: list[str] = []
new_current_nodes: list[str] = []
steps: int = 0

direction_value: dict[str, int] = {
    "L": 0,
    "R": 1,
}

# Reads instructions and nodes
with open("08-input.txt") as input_file:
    for line in input_file:
        line = line.strip()
        # If not empty line
        if line:
            if "=" in line:
                # It is a node
                node, edges = line.split(" = ")
                left_edge, right_edge = edges[1:-1].split(", ")
                nodes[node] = (left_edge, right_edge)
                # If the node ends in A add it to the current nodes list
                if node[-1] == "A":
                    current_nodes.append(node)
            else:
                # It is the instructions
                instructions = line
                number_instructions = len(instructions)

# Test Data
# instructions = "LR"
# number_instructions = len(instructions)
# nodes = {
#     "11A": ("11B", "XXX"),
#     "11B": ("XXX", "11Z"),
#     "11Z": ("11B", "XXX"),
#     "22A": ("22B", "XXX"),
#     "22B": ("22C", "22C"),
#     "22C": ("22Z", "22Z"),
#     "22Z": ("22B", "22B"),
#     "XXX": ("XXX", "XXX"),
# }
# current_nodes = ["11A", "22A"]

# Start at the first instruction
instruction_index = 0
# We do not start in z_ending nodes
z_ending = False

# While not on only "Z"-ending nodes
while not z_ending:
    # Suppose all are "Z"-ending
    z_ending = True
    # Increment the number of steps
    steps += 1
    # Clear the new current nodes list
    new_current_nodes = []
    # print(f"{current_nodes=}")
    # Follow current instruction for each current_node
    for current_node in current_nodes:
        # Find new node
        node = nodes[current_node]
        new_current_node = node[direction_value[instructions[instruction_index]]]
        # If the node doesn't end in "Z" then we will continue advancing
        if new_current_node[-1] != "Z":
            z_ending = False
        # print(
        #     f"{steps=}; {node=}; {instructions[instruction_index]=}; {new_current_node=}; {z_ending=}"
        # )
        # Add the new node to the new_current_nodes list
        new_current_nodes.append(new_current_node)
    # Update the current nodes list
    current_nodes = new_current_nodes
    # Increment the instruction index, returning to 0 if needed
    instruction_index = (instruction_index + 1) % number_instructions
    if steps % 1000000 == 0:
        print(f"{steps=}")

print(f"{steps=}")
