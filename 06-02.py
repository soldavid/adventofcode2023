time = 0
record_distance = 0
winning = 0

with open("06-input.txt") as input_file:
    for line in input_file:
        line = line.strip()  # Remove leading/trailing whitespace
        kind, values_field = line.split(":")
        value = int(values_field.replace(" ", ""))
        if kind == "Time":
            time = value
        if kind == "Distance":
            record_distance = value
for speed in range(1, time):
    distance = (time - speed) * speed
    if distance > record_distance:
        winning += 1
print(winning)
